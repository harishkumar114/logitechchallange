# Logitech Android Challenge

### Please parse the Json file from

    https://s3.amazonaws.com/harmony-recruit/devices.json


----------
{"devices":[{"deviceType":"tv","model":"la22c450e1","name":"samsung"},{"deviceType":"washing machine","model":"lg34gfer","name":"lg"},{"deviceType":"monitor","model":"dlrtio78","name":"dell"},{"deviceType":"tv","model":"sie45vgty","name":"sansui"},{"deviceType":"tv","model":"psqm903xf","name":"panasonic"},{"deviceType":"dvd player","model":"sd1266cg5","name":"sony"},{"deviceType":"avr","model":"yqmlri8v45","name":"yamaha"},{"deviceType":"xbox","model":"mx65dk893x","name":"Microsoft"},{"deviceType":"music","model":"sl9xaqnh4cf","name":"Sonos"},{"deviceType":"stream player","model":"r3klaq19nv","name":"Roku"}]}

----------


This has array of json objects. Display each object as a row in ListView. Each row should display the Name (read name key in json object, please check sample response in next page)

You have to demonstrate the
* i) OOPS concepts with proper abstraction
* ii) Android Skills. Do not use Async task to parse the file(instead use separate thread)
* iii) Threading (notifying UI thread from non UI thread)



***While writing the code please consider yourself as a full time employee of Logitech and your code is going to production. Follow all coding standards (of production quality code) with proper error handling etc...***

### How To:
* When you launch the application it will automatically get data from the server.
* From Next time you can update the content by pulling down the list(Like in gmail...).
* If any error occoured Toast is displayed.

Image 1 - On start on the application.

![Screenshot_1.png](https://bitbucket.org/repo/kLjnGp/images/2161978052-Screenshot_1.png)


Image 2 - After fetching and processing the content we display it in screen.

![Screenshot_2.png](https://bitbucket.org/repo/kLjnGp/images/3354017338-Screenshot_2.png)


Image 3 - Swipe from top to get the updated content from server again.

![Screenshot_3.png](https://bitbucket.org/repo/kLjnGp/images/4134818916-Screenshot_3.png)

#  

#### Thanks