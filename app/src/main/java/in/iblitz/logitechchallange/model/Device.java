package in.iblitz.logitechchallange.model;

/**
 * Created by Harish on 10/06/16.
 */
public class Device {
    private String name;
    private String model;
    private String deviceType;

    public String getDeviceType() {
        return capsFirst(deviceType);
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = capsFirst(deviceType);
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getName() {
        return capsFirst(name);
    }

    public void setName(String name) {
        this.name = capsFirst(name);
    }

    @Override
    public String toString() {
        return "Device{" +
                "name='" + name + '\'' +
                ", model='" + model + '\'' +
                ", deviceType='" + deviceType + '\'' +
                '}';
    }

    /**
     * Convert First letter of each word to capital.
     * @param str
     * @return
     */
    private String capsFirst(String str) {
        String[] words = str.split(" ");
        StringBuilder ret = new StringBuilder();
        for(int i = 0; i < words.length; i++) {
            ret.append(Character.toUpperCase(words[i].charAt(0)));
            ret.append(words[i].substring(1));
            if(i < words.length - 1) {
                ret.append(' ');
            }
        }
        return ret.toString();
    }
}
