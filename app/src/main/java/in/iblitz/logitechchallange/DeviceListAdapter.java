package in.iblitz.logitechchallange;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import in.iblitz.logitechchallange.model.Device;

/**
 * Created by Harish on 10/06/16.
 */
public class DeviceListAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<Device> devicesList = new ArrayList<>();

    public DeviceListAdapter(Context context) {
        this.context = context;
    }

    public ArrayList<Device> getDevicesList() {
        return devicesList;
    }

    public void setDevicesList(ArrayList<Device> devicesList) {
        this.devicesList = devicesList;
    }


    @Override
    public int getCount() {
        return devicesList.size();
    }

    @Override
    public Device getItem(int position) {
        return devicesList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if(convertView == null)
            convertView = inflater.inflate(R.layout.device_info_row, null);

        if(position % 2 == 0)
            convertView.setBackgroundColor(Color.parseColor("#f2f3f4"));
        else
            convertView.setBackgroundColor(Color.parseColor("#f0ffff"));

        TextView name = (TextView)convertView.findViewById(R.id.txt_name);
        TextView model = (TextView)convertView.findViewById(R.id.txt_model);
        TextView type = (TextView)convertView.findViewById(R.id.txt_type);

        Device currentItem = getItem(position);
        name.setText(currentItem.getName());
        model.setText(currentItem.getModel());
        type.setText(currentItem.getDeviceType());

        return convertView;
    }
}
