package in.iblitz.logitechchallange;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;

import in.iblitz.logitechchallange.model.Device;

public class MainActivity extends AppCompatActivity {

    /**
     * TIME IN MILLISECONDS
     **/
    private static final int CONNECTION_TIMEOUT = 20000;
    private static final int SUCCESS = 1001;
    private static final int DATA_ERROR = 9001;
    private static final int CLIENT_ERROR = 9004;
    private static final int SERVER_ERROR = 9008;
    private static final int UNKNOWN_ERROR = 9009;

    private static Handler mHandler;

    private SwipeRefreshLayout swipeRefreshLayout;
    private ListView modelsListView;
    private DeviceListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh);
        modelsListView = (ListView) findViewById(R.id.list_models);

//        Handler to update UI on data refresh.
        mHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
//                super.handleMessage(msg);
                swipeRefreshLayout.setRefreshing(false);
                switch (msg.what) {
                    case SUCCESS:
                        if (adapter == null) {
                            adapter = new DeviceListAdapter(MainActivity.this);
                            adapter.setDevicesList((ArrayList<Device>) msg.obj);
                            modelsListView.setAdapter(adapter);
                        } else {
                            adapter.setDevicesList((ArrayList<Device>) msg.obj);
                            adapter.notifyDataSetChanged();
                        }
                        break;
                    case DATA_ERROR:
                        Toast.makeText(MainActivity.this, getString(R.string.data_error), Toast.LENGTH_SHORT).show();
                        break;
                    case CLIENT_ERROR:
                        Toast.makeText(MainActivity.this, getString(R.string.client_error), Toast.LENGTH_SHORT).show();
                        break;
                    case SERVER_ERROR:
                        Toast.makeText(MainActivity.this, getString(R.string.server_error), Toast.LENGTH_SHORT).show();
                        break;
                    case UNKNOWN_ERROR:
                        Toast.makeText(MainActivity.this, getString(R.string.unknown_error), Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        };

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (isInternetAvailable()) {
                    getModelsFromServer();
                } else {
                    swipeRefreshLayout.setRefreshing(false);
                    Toast.makeText(MainActivity.this, getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                }
            }
        });

        swipeRefreshLayout.post(
            new Runnable() {
                @Override
                public void run() {
                    swipeRefreshLayout.setRefreshing(true);
                    getModelsFromServer();
                }
            }
        );
    }

    private void getModelsFromServer() {
        swipeRefreshLayout.setRefreshing(true);
        Thread webRequest = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    HttpURLConnection connection = (HttpURLConnection)
                            new URL("https://s3.amazonaws.com/harmony-recruit/devices.json").openConnection();
                    connection.setConnectTimeout(CONNECTION_TIMEOUT /* milliseconds */);
                    connection.setRequestMethod("GET");
                    connection.connect();

                    int responseCode = connection.getResponseCode();
                    if (responseCode == HttpURLConnection.HTTP_OK) {
                        BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
//                        Read line from file and copy to String buffer object.
                        StringBuilder responseData = new StringBuilder();
                        String line;
                        while ((line = br.readLine()) != null) {
                            responseData.append(line + "\n");
                        }
                        Log.d("MainActivity", "Response: " + responseData);
                        Device[] deviceList = (Device[]) new Gson().fromJson(new JSONObject(responseData.toString()).getString("devices"), Device[].class);
//                        Message object to send response data
                        Message msg = new Message();
                        msg.obj = new ArrayList(Arrays.asList(deviceList));
                        msg.what = SUCCESS;
                        mHandler.sendMessage(msg);
                    } else if (responseCode >= 400 && responseCode <= 499) {
//                        Error from client side
                        mHandler.sendEmptyMessage(CLIENT_ERROR);
                    } else if (responseCode >= 500 && responseCode <= 526) {
//                        Error with server
                        mHandler.sendEmptyMessage(SERVER_ERROR);
                    } else {
                        mHandler.sendEmptyMessage(UNKNOWN_ERROR);
                    }
                } catch (IOException ioe) {
                    ioe.printStackTrace();
                    mHandler.sendEmptyMessage(UNKNOWN_ERROR);
                } catch (JSONException je) {
                    mHandler.sendEmptyMessage(DATA_ERROR);
                    je.printStackTrace();
                } catch (Exception e) {
                    mHandler.sendEmptyMessage(UNKNOWN_ERROR);
                    e.printStackTrace();
                }
            }
        });
        webRequest.start();
    }


    /**
     * Check if device is connected to internet.
     *
     * @return <B>True<B/> - If connected to internet (or) <B>False</B>
     */
    private boolean isInternetAvailable() {
        ConnectivityManager connMgr = (ConnectivityManager)
                getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            return true;
        } else {
            return false;
        }
    }
}
